# Image Classification Machine Learning App

App available in: https://progamandoconro.github.io/Image-Classification-ML-App/

## Features: Upload and classify an image with Machine Learning in the browser.

## Clone this repository

```git clone https://gitlab.com/progamandoconro/Image-Classification-ML-App```

## Install dependencies and start the App

```
cd Image-Classification-ML-App
npm install
npm start 

```
